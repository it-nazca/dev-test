# Nazca Tech & Consulting

## Prueba de Desarrollo: Arrays

Conocimiento de las acciones de conversion de datos básicos

## 01 - Operaciones Lineales

Dado el arreglo de datos

```PHP
$array = [1,3,2,3,4,5,6,7];
```

Obtener:

- Usando la función `array_reduce` calcular el acumulado de los valores de `$array`
- Usando la función `array_filter` obtener solo los números impares de los valores de `$array`
- Ordenar de forma inversa los valores de `$array`

## 02 - Operaciones Matriciales

Dada la matriz:

``` PHP
$matrix = [
    ['user'=>'A', 'name'=>'a a', 'age'=>17],
    ['user'=>'B', 'name'=>'b b', 'age'=>22],
    ['user'=>'C', 'name'=>'c c', 'age'=>36],
    ['user'=>'D', 'name'=>'d d', 'age'=>42],
    ['user'=>'E', 'name'=>'e e', 'age'=>12],
];
```

Obtener:

- Usando la función `array_reduce` calcular promedio de edades de `$matrix`
- Usando la función `array_filter` solo los mayores de 30 años `$matrix`
- iterar dentro de un loop `foreach` para mostrar la data en una tabla;
- Usando la función `array_map` transformar cada indice dentro de `$matrix` para agregar la marca de mayor de edad

    ```PHP
     ['user'=>'zzzz', 'name'=>'z z', 'age'=>99, 'adult'=> true],
     ```

    y guardar en una nueva variable `$matrix_calc`

- Convertir a formato json string la matriz `$matrix_calc`
