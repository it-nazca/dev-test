# Nazca Tech & Consulting

## Prueba de Desarrollo: Data Manipulation

Conocimiento de las acciones de conversion de datos básicos

## 01 - Números

Dado de número `$num = 1324223.1365593`

Obtener:

- Parte entera
- Parte decimal
- Redondeo a dos decimales
- Redondeo Superior
- Redondeo Inferior
- Numero con formato (dos decimales) ##.###,##

## 02 - Cadenas

Dada la cadena `/Respuestas/PHP/DataManipulation/02/`

Obtener:

- Conteo de caracteres
- Conteo de Letras
- Conteo de Letras solo en Mayúscula
- Reemplazar las palabras a formato `snake_case`
