# Nazca Tech & Consulting

## Prueba de Desarrollo: Laravel

Control de Objetos y Organización nombres de espacio

## 01 - CRUD

Crear un app base para guardar contactos que permita crear, editar y eliminar los registros de un listado.

La tabla de contactos debe ser creada con una migración y la estructura debe ser la siguiente

- `contacts`
  - (String) `name`
  - (String) `email`
  - (String) `phone`
  - (Int) `age`
  - (Bool) `active` | default(`true`)

Debe incluir los timestamps por defecto `created_at` y `updated_at`

La operaciones del Crud deben ser completadas en base a un modelo `Contacts.php`

> - Por defecto se debe filtrar las filas que estén activas(`active` = `true`)
> - Para modificar esta columna debe ser en la acción de editar
> - No es necesario un ciclo de login
